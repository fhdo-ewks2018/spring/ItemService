package de.fhdortmund.fb4.ewks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The spring boot starting point.
 *
 * @author Stefan S.
 */
@SpringBootApplication
public class ItemServiceApplication {

	/**
	 * The standard entry point for a java executable.
	 *
	 * @param args
	 *            The command line arguments.
	 */
	public static void main(String[] args) {
		SpringApplication.run(ItemServiceApplication.class, args);
	}
}
