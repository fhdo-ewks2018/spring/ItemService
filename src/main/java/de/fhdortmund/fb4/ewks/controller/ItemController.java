package de.fhdortmund.fb4.ewks.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.fhdortmund.fb4.ewks.model.Item;
import de.fhdortmund.fb4.ewks.model.ItemRepository;
import de.fhdortmund.fb4.ewks.view.StatusCodeException;
import io.micrometer.core.annotation.Timed;

/**
 * The rest controller for this service.
 *
 * @author Stefan S.
 */
@RestController("/items")
@Timed
public class ItemController {

	/**
	 * The repository for storing and retrieving items from the database.
	 */
	@Autowired
	private ItemRepository itemRepository;

	/**
	 * Get a list of all the {@link Item}s that the service knows about.
	 *
	 * @return A list of all the {@link Item}s.
	 */
	@RequestMapping
	public ResponseEntity<List<Item>> getItems(@RequestParam(name = "ids", required = false) List<String> listOfIds) {
		if (listOfIds == null) {
			return new ResponseEntity<>(itemRepository.findAll(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(itemRepository.findAllById(listOfIds), HttpStatus.OK);
		}
	}

	/**
	 * Add a new {@link Item} to the service.
	 *
	 * @param item
	 *            The item to add to the service.
	 * @return The added item.
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Item> postItem(@RequestBody Item item) {
		if (item.getId() != null && !item.getId().isEmpty()) {
			throw new StatusCodeException(HttpStatus.BAD_REQUEST, "The id mustn't be set.");
		}

		Item savedItem = itemRepository.save(item);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", "/items/" + savedItem.getId());

		return new ResponseEntity<>(savedItem, headers, HttpStatus.CREATED);
	}

	/**
	 * Get a single {@link Item} from this service.
	 *
	 * @param id
	 *            The id of the {@link Item}
	 * @return The {@link Item}
	 */
	@RequestMapping(path = "/items/{id}")
	public ResponseEntity<Item> getItem(@PathVariable("id") String id) {
		Optional<Item> item = itemRepository.findById(id);

		if (!item.isPresent()) {
			throw new StatusCodeException(HttpStatus.NOT_FOUND,
					String.format("The object with the id '%s' couldn't be found.", id));
		}

		return new ResponseEntity<>(item.get(), HttpStatus.OK);
	}

	/**
	 * Update an item with new values.
	 *
	 * @param id
	 *            The id of the {@link Item} to change
	 * @param item
	 *            The new {@link Item} to put
	 * @return The written {@link Item}
	 */
	@RequestMapping(path = "/items/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Item> putItem(@PathVariable("id") String id, @RequestBody Item item) {
		if (item.getId() != null && !item.getId().isEmpty()) {
			throw new StatusCodeException(HttpStatus.BAD_REQUEST, "The id mustn't be set.");
		}

		Optional<Item> foundItem = itemRepository.findById(id);

		if (!foundItem.isPresent()) {
			throw new StatusCodeException(HttpStatus.NOT_FOUND,
					String.format("The object with the id '%s' couldn't be found.", id));
		}

		foundItem.ifPresent((containedItem) -> {
			containedItem.setName(item.getName());
			containedItem.setSize(item.getSize());
			containedItem.setWeight(item.getWeight());
		});

		Item savedItem = itemRepository.save(foundItem.get());

		return new ResponseEntity<>(savedItem, HttpStatus.OK);
	}

	/**
	 * Delete an {@link Item} from this service.
	 *
	 * @param id
	 *            The id of the {@link Item} to delete
	 */
	@RequestMapping(path = "/items/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteItem(@PathVariable("id") String id) {
		itemRepository.deleteById(id);

		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}
}
