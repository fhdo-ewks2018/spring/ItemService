package de.fhdortmund.fb4.ewks.model;

import org.springframework.data.annotation.Id;

/**
 * A bean representing an item.
 *
 * @author Stefan S.
 */
public class Item {

	/**
	 * The Mongo ID
	 */
	@Id
	private String id;

	/**
	 * Name of the item
	 */
	private String name;

	/**
	 * Weight of the item
	 */
	private double weight;

	/**
	 * Size of the item (in cm³)
	 */
	private double size;

	/**
	 * Empty constructor
	 */
	public Item() {
		super();
	}

	/**
	 * Constructor for easy building.
	 *
	 * @param name
	 *            The name of the Item
	 * @param weight
	 *            The weight of the item
	 * @param size
	 *            The size of the item (in cm³)
	 */
	public Item(String name, double weight, double size) {
		this.name = name;
		this.weight = weight;
		this.size = size;
	}

	/**
	 * Constructor for easy testing.
	 *
	 * @param id
	 *            The id of the Item
	 * @param name
	 *            The name of the Item
	 * @param weight
	 *            The weight of the item
	 * @param size
	 *            The size of the item (in cm³)
	 */
	public Item(String id, String name, double weight, double size) {
		this.id = id;
		this.name = name;
		this.weight = weight;
		this.size = size;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}

	/**
	 * @return the size
	 */
	public double getSize() {
		return size;
	}

	/**
	 * @param size
	 *            the size to set
	 */
	public void setSize(double size) {
		this.size = size;
	}
}
