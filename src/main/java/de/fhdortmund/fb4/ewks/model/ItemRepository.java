package de.fhdortmund.fb4.ewks.model;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * The spring {@link MongoRepository} for reading and writing {@link Item}s to
 * and from the database.
 *
 * @author Stefan S.
 */
public interface ItemRepository extends MongoRepository<Item, String> {
	/*
	 * (non-Javadoc)
	 *
	 * @see org.springframework.data.repository.CrudRepository#findAllById(java.lang
	 * .Iterable)
	 */
	@Override
	List<Item> findAllById(Iterable<String> ids);
}
