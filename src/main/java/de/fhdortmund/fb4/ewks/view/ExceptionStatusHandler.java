package de.fhdortmund.fb4.ewks.view;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * A class to add better error handling to this service.
 *
 * @author Stefan S.
 */
@ControllerAdvice
public class ExceptionStatusHandler {

	/**
	 * Handles the {@link Exception}s and returns the status as well as the error
	 * message to the caller.
	 *
	 * @param exception
	 *            The exception from which the response is generated.
	 * @return The response for the caller.
	 */
	@ExceptionHandler(StatusCodeException.class)
	public ResponseEntity<String> handleException(StatusCodeException exception) {
		// TODO: Log exception with logging server
		return ResponseEntity.status(exception.getHttpStatus()).body(exception.getMessage());
	}
}
