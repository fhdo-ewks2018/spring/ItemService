package de.fhdortmund.fb4.ewks.view;

import org.springframework.http.HttpStatus;

/**
 * An {@link Exception} that encodes a {@link HttpStatus} for building an error
 * response.
 *
 * @author Stefan S
 */
public class StatusCodeException extends RuntimeException {

	/**
	 * Generated id for serialization.
	 */
	private static final long serialVersionUID = -2584184400191211694L;

	/**
	 * The status of the request.
	 */
	private HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	/**
	 * Generates an error message for the caller.
	 *
	 * @param httpStatus
	 *            The status of the request.
	 * @param errorMessage
	 *            The error message shown to the caller.
	 */
	public StatusCodeException(HttpStatus httpStatus, String errorMessage) {
		super(errorMessage);
		this.httpStatus = httpStatus;
	}

	/**
	 * Getter for the {@link HttpStatus} of the request.
	 *
	 * @return The status of the request.
	 */
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}
