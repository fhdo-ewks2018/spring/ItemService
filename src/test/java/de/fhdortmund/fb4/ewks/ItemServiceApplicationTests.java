package de.fhdortmund.fb4.ewks;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.fhdortmund.fb4.ewks.controller.ItemController;
import de.fhdortmund.fb4.ewks.model.Item;
import de.fhdortmund.fb4.ewks.model.ItemRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemServiceApplicationTests {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ItemRepository itemRepository;

	@Test
	public void getAllItems() throws Exception {
		List<Item> itemList = new ArrayList<>();

		itemList.add(new Item("1", "Arcane Focus", 1.4, 0.5));
		itemList.add(new Item("2", "Sword", 6, 1));
		itemList.add(new Item("3", "Leather Armor", 20, 50));

		when(itemRepository.findAll()).thenReturn(itemList);

		mockMvc.perform(get("/items")).andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$", hasSize(3))).andExpect(jsonPath("$[0].id", is(itemList.get(0).getId())))
				.andExpect(jsonPath("$[0].name", is(itemList.get(0).getName())))
				.andExpect(jsonPath("$[0].weight", is(itemList.get(0).getWeight())))
				.andExpect(jsonPath("$[0].size", is(itemList.get(0).getSize())))
				.andExpect(jsonPath("$[1].id", is(itemList.get(1).getId())))
				.andExpect(jsonPath("$[1].name", is(itemList.get(1).getName())))
				.andExpect(jsonPath("$[1].weight", is(itemList.get(1).getWeight())))
				.andExpect(jsonPath("$[1].size", is(itemList.get(1).getSize())))
				.andExpect(jsonPath("$[2].id", is(itemList.get(2).getId())))
				.andExpect(jsonPath("$[2].name", is(itemList.get(2).getName())))
				.andExpect(jsonPath("$[2].weight", is(itemList.get(2).getWeight())))
				.andExpect(jsonPath("$[2].size", is(itemList.get(2).getSize())));

		verify(itemRepository).findAll();
		verify(itemRepository, never()).findAllById(any());
	}

	@Test
	public void getSpecificItems() throws Exception {
		List<Item> itemList = new ArrayList<>();

		itemList.add(new Item("1", "Arcane Focus", 1.4, 0.5));
		itemList.add(new Item("2", "Sword", 6, 1));
		itemList.add(new Item("3", "Leather Armor", 20, 50));

		when(itemRepository.findAllById(any())).thenReturn(itemList);

		mockMvc.perform(get("/items").param("ids", itemList.get(0).getId()).param("ids", itemList.get(1).getId())
				.param("ids", itemList.get(2).getId())).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(jsonPath("$", hasSize(3)))
				.andExpect(jsonPath("$[0].id", is(itemList.get(0).getId())))
				.andExpect(jsonPath("$[0].name", is(itemList.get(0).getName())))
				.andExpect(jsonPath("$[0].weight", is(itemList.get(0).getWeight())))
				.andExpect(jsonPath("$[0].size", is(itemList.get(0).getSize())))
				.andExpect(jsonPath("$[1].id", is(itemList.get(1).getId())))
				.andExpect(jsonPath("$[1].name", is(itemList.get(1).getName())))
				.andExpect(jsonPath("$[1].weight", is(itemList.get(1).getWeight())))
				.andExpect(jsonPath("$[1].size", is(itemList.get(1).getSize())))
				.andExpect(jsonPath("$[2].id", is(itemList.get(2).getId())))
				.andExpect(jsonPath("$[2].name", is(itemList.get(2).getName())))
				.andExpect(jsonPath("$[2].weight", is(itemList.get(2).getWeight())))
				.andExpect(jsonPath("$[2].size", is(itemList.get(2).getSize())));

		verify(itemRepository).findAllById(any());
		verify(itemRepository, never()).findAll();
	}

	@Test
	public void getNonExistentItems() throws Exception {
		when(itemRepository.findAllById(any())).thenReturn(new ArrayList<>());

		mockMvc.perform(get("/items").param("ids", "invalid")).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(jsonPath("$", hasSize(0)));

		verify(itemRepository).findAllById(any());
		verify(itemRepository, never()).findAll();
	}

	@Test
	public void getOneItem() throws Exception {
		Item testItem = new Item("1", "Arcane Focus", 1.4, 0.5);

		when(itemRepository.findById(testItem.getId())).thenReturn(Optional.of(testItem));

		mockMvc.perform(get("/items/" + testItem.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(contentType)).andExpect(jsonPath("$.id", is(testItem.getId())))
				.andExpect(jsonPath("$.name", is(testItem.getName())))
				.andExpect(jsonPath("$.weight", is(testItem.getWeight())))
				.andExpect(jsonPath("$.size", is(testItem.getSize())));
	}

	@Test
	public void getItemNotFound() throws Exception {
		when(itemRepository.findById("invalid")).thenReturn(Optional.empty());

		mockMvc.perform(get("/items/invalid")).andExpect(status().isNotFound())
				.andExpect(content().string(containsString("'invalid'")));
	}

	@Test
	public void postItem() throws Exception {
		Item testItem = new Item("1", "New Item", 15, 1.6);

		when(itemRepository.save(any(Item.class))).thenReturn(testItem);

		String itemJson = json(new Item(testItem.getName(), testItem.getWeight(), testItem.getSize()));

		mockMvc.perform(post("/items").contentType(contentType).content(itemJson)).andExpect(status().isCreated())
				.andExpect(header().string("Location", "/items/" + testItem.getId()))
				.andExpect(content().contentType(contentType)).andExpect(jsonPath("$.id", is(testItem.getId())))
				.andExpect(jsonPath("$.name", is(testItem.getName())))
				.andExpect(jsonPath("$.weight", is(testItem.getWeight())))
				.andExpect(jsonPath("$.size", is(testItem.getSize())));
	}

	@Test
	public void postItemWithId() throws Exception {
		String itemJson = json(new Item("invalid", "New Item", 15, 1.6));

		mockMvc.perform(post("/items").contentType(contentType).content(itemJson)).andExpect(status().isBadRequest());
	}

	@Test
	public void putItem() throws Exception {
		Item testItem = new Item("1", "Arcane Focus", 1.4, 0.5);

		when(itemRepository.findById(testItem.getId())).thenReturn(Optional.of(testItem));
		when(itemRepository.save(any(Item.class))).thenReturn(testItem);

		String itemJson = json(new Item("UpdatedItem", 1.5, 12));

		mockMvc.perform(put("/items/" + testItem.getId()).contentType(contentType).content(itemJson))
				.andExpect(status().isOk()).andExpect(content().contentType(contentType))
				.andExpect(jsonPath("$.id", is(testItem.getId()))).andExpect(jsonPath("$.name", is(testItem.getName())))
				.andExpect(jsonPath("$.weight", is(testItem.getWeight())))
				.andExpect(jsonPath("$.size", is(testItem.getSize())));
	}

	@Test
	public void putItemWithId() throws Exception {
		String itemJson = json(new Item("invalid", "New Item", 15, 1.6));

		mockMvc.perform(put("/items/valid").contentType(contentType).content(itemJson))
				.andExpect(status().isBadRequest()).andExpect(header().doesNotExist("Location"));
	}

	@Test
	public void putNonExistentItem() throws Exception {
		String itemJson = json(new Item("New Item", 15, 1.6));

		when(itemRepository.findById("invalid")).thenReturn(Optional.empty());

		mockMvc.perform(put("/items/invalid").contentType(contentType).content(itemJson))
				.andExpect(status().isNotFound());
	}

	@Test
	public void deleteItem() throws Exception {
		Item testItem = new Item("1", "Arcane Focus", 1.4, 0.5);

		mockMvc.perform(delete("/items/" + testItem.getId())).andExpect(status().isAccepted());

		verify(itemRepository).deleteById(testItem.getId());
	}

	@Test
	public void deleteNonExistentItem() throws Exception {
		mockMvc.perform(delete("/items/invalid")).andExpect(status().isAccepted());

		verify(itemRepository).deleteById("invalid");
	}

	protected String json(Item item) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		return mapper.writeValueAsString(item);
	}

}
